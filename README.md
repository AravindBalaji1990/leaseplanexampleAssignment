This is a Automation project to test the API with different products.

Base API:https://waarkoop-server.herokuapp.com/api/v1/search/test/{product}
Path parameter : {product} from base url will accept values like apple,mango and is case sensitive

What are the Technologies used?
The technoloigy used to implement the automation framework are as below, 
1.Serenity
2.Java
3.Cucumber with serenity integration
4.Restassured with serenity integration
5.Maven & Gradle - dependency management and build management

How do I execute the Automation Script?
1.Download the zip file 
2.Extract the package 
3.Import the package in Eclipe/Intellij 
4.Navigate to pom.xml updated/download the dependencies
5.Build the gradle
6.Open the terminal navigate to the path of build.gradle 
5.Execute the command "gradle test" 

How to View the Report? 
1.After the script Runs 
2.Refresh the project we see a folder "\target\site\serenity\index.html" in the project path 
3.Open the index.html which will launch the pass/fail results
Note: Sample results are available in repo.

what was refractored?
1.Feature file was changed from scenario to scenario outline to accomodate more scenarios with change in data 
2.Created resuable step definition to minimize code change trued to maintain 'O' from SOLID principle
3.build.gradle needed dependency "net.serenity-bdd:serenity-rest-assured"
4.pom.xml needed dependency "serenity-rest-assured"
5.TestRunner file has missing glue added it
6.Step definitions wasnt identified so need to refractors it as well

